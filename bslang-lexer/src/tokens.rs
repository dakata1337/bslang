#[derive(Debug, PartialEq)]
#[allow(dead_code)]
pub enum Numbers {
    // 69.420
    Float(f64),
    // 1337
    Int(i64),
}

#[derive(Debug, PartialEq)]
#[allow(dead_code)]
pub enum Tokens {
    // let
    Let,
    // =
    Assign,
    // ==
    Equals,
    // >
    GreaterThan,
    // <
    LessThan,

    // *
    Asterisk,
    // /
    ForwardSlash,

    // (
    OpenBracket,
    // )
    CloseBracket,
    // ;
    Semicolon,

    Number(Numbers),
    // "Hello, World"
    String(String),
    // // My double forward slash comment
    Comment(String),
    // Name of variable/function
    Identifier(String),
}
