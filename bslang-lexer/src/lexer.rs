use crate::tokens::{Numbers, Tokens};

pub struct Lexer {
    content: Vec<char>,
    current_char_idx: usize,
}

fn is_digit(ch: char) -> bool {
    ('0'..='9').contains(&ch)
}

/*
 * TODO: replace Option<Token> with Result<Token, Err>
 * TODO: handle functions `fn my_func(arg1, arg2, ...) { ... }` and `my_func("hello");`
 * */

impl Lexer {
    pub fn new(content: String) -> Self {
        Self {
            content: content.chars().collect(),
            current_char_idx: 0,
        }
    }

    pub fn collect(&mut self) -> Vec<Tokens> {
        let mut tokens = vec![];
        while let Some(tok) = self.next_token() {
            tokens.push(tok);
        }
        tokens
    }

    fn skip_whitespace(&mut self) {
        while let Some(ch) = self.next_char() {
            match ch {
                ' ' | '\t' | '\n' => continue,
                _ => break,
            }
        }
        self.current_char_idx -= 1;
    }

    fn next_char(&mut self) -> Option<char> {
        if self.current_char_idx >= self.content.len() {
            None
        } else {
            let ch = self.content[self.current_char_idx];
            self.current_char_idx += 1;
            Some(ch)
        }
    }

    fn get_single_line_comment(&mut self) -> String {
        let mut buf = String::with_capacity(512);
        while let Some(ch) = self.next_char() {
            match ch {
                '\n' => break,
                _ => buf.push(ch),
            }
        }
        buf
    }

    fn get_number(&mut self) -> Option<Numbers> {
        let mut buf = String::with_capacity(128);
        let mut is_float = false;
        while let Some(ch) = self.next_char() {
            if ch == '.' {
                if is_float {
                    panic!("Invalid float");
                }
                is_float = true;
            } else if !is_digit(ch) {
                self.current_char_idx -= 1;
                break;
            }
            buf.push(ch);
        }

        let number = if is_float {
            Numbers::Float(buf.parse().unwrap())
        } else {
            Numbers::Int(buf.parse().unwrap())
        };
        Some(number)
    }

    fn get_string(&mut self) -> String {
        let mut buf = String::with_capacity(512);
        let mut prev: Option<char> = None;
        while let Some(ch) = self.next_char() {
            if prev.is_some() && prev.unwrap() != '\\' && ch == '"' {
                break;
            }
            buf.push(ch);
            prev = Some(ch);
        }
        buf
    }

    pub fn next_token(&mut self) -> Option<Tokens> {
        self.skip_whitespace();

        let mut buf = String::with_capacity(512);
        while let Some(ch) = self.next_char() {
            if is_digit(ch) {
                self.current_char_idx -= 1;
                if let Some(num) = self.get_number() {
                    return Some(Tokens::Number(num));
                }
            }

            match ch {
                ' ' | '\t' | '\n' => break,
                '(' => {
                    if !buf.is_empty() {
                        self.current_char_idx -= 1;
                        break;
                    }
                    return Some(Tokens::OpenBracket);
                }
                ')' => return Some(Tokens::CloseBracket),
                '"' => return Some(Tokens::String(self.get_string())),
                _ => buf.push(ch),
            }
        }

        if buf.len() > 1 && buf.ends_with(';') {
            self.current_char_idx -= 2;
            buf.pop();
        }

        if buf.is_empty() {
            None
        } else {
            Some(match buf.trim() {
                "//" => Tokens::Comment(self.get_single_line_comment()),
                "let" => Tokens::Let,
                "=" => Tokens::Assign,
                "==" => Tokens::Equals,
                ">" => Tokens::GreaterThan,
                "<" => Tokens::LessThan,
                ";" => Tokens::Semicolon,
                "*" => Tokens::Asterisk,
                "/" => Tokens::ForwardSlash,
                _ => Tokens::Identifier(buf),
            })
        }
    }
}
