use std::{fs, time::Instant};

use bslang_lexer::{lexer::Lexer, tokens::Tokens};

fn main() {
    let file = fs::read_to_string("../examples/basics.bslang").expect("Failed to open file");
    let start = Instant::now();

    let mut lexer = Lexer::new(file);

    let tokens = lexer.collect();
    for tok in tokens {
        match tok {
            Tokens::Semicolon => println!("{:?}\n", tok),
            _ => println!("{:?}", tok),
        }
    }

    let end = Instant::now();
    println!("Elapsed time: {:?}", end - start);
}
